// Captura el evento del icono bars

let openMenuEvent = bars => {

    bars.addEventListener('click', e => {
        e.preventDefault();
        console.log("click!!");
        openMenu();
    });
}

// funcion que crea el menu 

let openMenu = () => {
    let menu = document.createElement('div');
    menu.classList.add('nav');
    menu.innerHTML = `
        <div class="close"><a class="close__link fas fa-times" href="#"></a></div>
        <ul class="nav__list">
            <li class="nav__item"><a href="#" class="nav__link">Home</a></li>
            <li class="nav__item"><a href="#" class="nav__link">Elements</a></li>
            <li class="nav__item"><a href="#" class="nav__link">Generic</a></li>
        </ul>
    `;

    document.body.appendChild(menu);

    closeMenu(menu);
}

// Funcion que cierra el menu

let closeMenu = menu => {
    let close = document.querySelector('.close__link');

    close.addEventListener('click', e => {
        e.preventDefault();
        document.body.removeChild(menu);
    })
}

// funcion que ejecuta las funciones de abrir y cerrar menu

let menu = () => {
    let bars = document.querySelector('.menu__icon');

    openMenuEvent(bars);
}

menu();